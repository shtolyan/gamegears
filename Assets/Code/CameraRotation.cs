using System;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] [Range(0, 10)] private float _lookAtHeight = 3; 
    [SerializeField] private Transform _anchor; 
    [SerializeField] [Range(0, 30)] private float _distance = 3f;

    private float _previousDistance;
    private Vector3 _previousPosition;

    private void Start()
    {
        _previousDistance = _distance;

        if (_anchor)
        {
            SetTarget();
        }
    }
    private void SetTarget()
    {
        _camera.transform.position = _camera.transform.forward * - _distance;
        MoveCamera(Vector3.zero, Vector3.zero);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _previousPosition = _camera.ScreenToViewportPoint(Input.mousePosition);
        }
        else if (Input.GetMouseButton(0))
        {
            Vector3 newPosition = _camera.ScreenToViewportPoint(Input.mousePosition);
            Vector3 direction = _previousPosition - newPosition;

            MoveCamera(newPosition, direction);
        }

        var scroll = Input.GetAxis("Mouse ScrollWheel");
        _distance += scroll;
        if (Math.Abs(_previousDistance - _distance) > 0.05f)
        {
            MoveCamera(_previousPosition, Vector3.zero);
        }
         
    }

    private void MoveCamera(Vector3 newPosition, Vector3 direction)
    {
        var rotationAroundYAxis = -direction.x * 180; 
        var rotationAroundXAxis = direction.y * 180; 
        
        _camera.transform.position = _anchor.position + Vector3.up * _lookAtHeight;
        _camera.transform.Rotate(new Vector3(1, 0, 0), rotationAroundXAxis);
        _camera.transform.Rotate(new Vector3(0, 1, 0), rotationAroundYAxis, Space.World);

        _camera.transform.Translate(new Vector3(0, 0, - _distance));

        _previousPosition = newPosition;
    }
}
