using System.Collections.Generic;
using UnityEngine;

namespace View
{
    [RequireComponent(typeof(Animator), typeof(AudioSource))]
    public class AnimatorView : PlayerView
    {
        [SerializeField] private AudioClip[] _hitSounds;
        public bool CanAttack => _canAttack;
        
        private Animator _animator;
        private bool _canAttack = true;
        private AudioSource _audioSource;
        private static readonly int _attack = Animator.StringToHash("Attack");
        private static readonly int _health = Animator.StringToHash("Health");

        protected override void OnInit()
        {
            _animator = gameObject.GetComponent<Animator>();
            _audioSource = gameObject.GetComponent<AudioSource>();
        }

        protected override void OnDamage(float totalDamage)
        {
            _animator.SetInteger(_health, (int) player.Life);
        }

        protected override void OnBuff(List<PlayerBuff> buffs)
        {
            _animator.SetInteger(_health, (int) player.Life);
        }

        protected override void OnReset()
        {
            _canAttack = true;
            _animator.SetInteger(_health, (int) player.Life);
        }

        /// <summary>
        /// Invoke from animator
        /// </summary>
        private void AttackComplete()
        {
            var clip = _hitSounds[Random.Range(0, _hitSounds.Length)];
            _audioSource.clip = clip;
            _audioSource.Play();
            _canAttack = true;
            player.AttackComplete();
        }

        public void Attack()
        {
            _canAttack = false;
            _animator.SetTrigger(_attack);
        }
    }
}