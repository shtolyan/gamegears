﻿using System.Collections.Generic;
using Game;
using UnityEngine;

namespace View
{
    public class StatsView : PlayerView
    {
        [SerializeField] private Transform _statsPanel;
        [SerializeField] private UIStat _uiStatPrefab;

        private readonly Dictionary<int, UIStat> _uiStats = new Dictionary<int, UIStat>();

        protected override void OnInit()
        {
            CreateStats();
            UpdateStats();
        }

        protected override void OnReset()
        {
            ClearStats();
        }

        protected override void OnBuff(List<PlayerBuff> buffs)
        {
            UpdateStats();
        }

        protected override void OnDamage(float totalDamage)
        {
            UpdateStats();
        }

        protected override void OnAttack(float stolenLife)
        {
            UpdateStats();
        }

        private void CreateStats()
        {
            foreach (var playerStat in player.Stats.values)
            {
                var uiStat = Instantiate(_uiStatPrefab, _statsPanel);
                var statData = GameWorld.GetStat(playerStat.id);
                _uiStats.Add(statData.id, uiStat);
            }
        }

        private void UpdateStats()
        {
            foreach (var playerStat in player.Stats.values)
            {
                var statData = GameWorld.GetStat(playerStat.id);
                var uiStat = _uiStats[statData.id];
                var statValue = player.GetStatValue(playerStat.id);
                uiStat.Set(statData.icon, statValue.ToString("N0"));
            }
        }

        private void ClearStats()
        {
            foreach (var uiStat in _uiStats.Values)
            {
                Destroy(uiStat.gameObject);
            }

            _uiStats.Clear();
        }
    }
}
