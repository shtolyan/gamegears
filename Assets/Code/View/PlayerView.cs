using System.Collections.Generic;
using Game;
using UnityEngine;

namespace View
{
    public class PlayerView : MonoBehaviour
    {
        protected Player player;

        public void Init(Player player)
        {
            this.player = player;
            this.player.AttackEvent += PlayerOnAttackEvent;
            this.player.BuffEvent += PlayerOnBuffEvent;
            this.player.DamageEvent += PlayerOnDamageEvent;
            this.player.ResetEvent += PlayerOnResetEvent;

            OnInit();
        }

        protected virtual void OnInit()
        {

        }

        protected virtual void OnReset()
        {

        }

        protected virtual void OnDamage(float damage)
        {

        }

        protected virtual void OnBuff(List<PlayerBuff> buffs)
        {

        }

        protected virtual void OnAttack(float stolenLife)
        {

        }

        private void PlayerOnResetEvent()
        {
            OnReset();
        }

        private void PlayerOnDamageEvent(float damage)
        {
            OnDamage(damage);
        }

        private void PlayerOnBuffEvent(List<PlayerBuff> buffs)
        {
            OnBuff(buffs);
        }

        private void PlayerOnAttackEvent(float stolenLife)
        {
            OnAttack(stolenLife);
        }
    }
}