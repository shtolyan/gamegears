﻿using UnityEngine;
using UnityEngine.UI;

namespace View
{
   public class UIStat : MonoBehaviour
   {
      [SerializeField] private Image _icon;
      [SerializeField] private Text _value;

      public void Set(string icon, string value)
      {
         _icon.sprite = IconCollection.Get(icon);
         _value.text = value;
      }
   }
}