using System.Collections.Generic;
using Game;
using UnityEngine;

namespace View
{
    public class BuffsView : PlayerView
    {
        [SerializeField] private Transform _statsPanel;
        [SerializeField] private UIStat uiStatPrefab;

        private readonly List<UIStat> _uiBuffs = new List<UIStat>();

        protected override void OnBuff(List<PlayerBuff> buffs)
        {
            foreach (var buff in buffs)
            {
                var uiBuff = Instantiate(uiStatPrefab, _statsPanel);
                var buffData = GameWorld.GetBuff(buff.id);
                uiBuff.Set(buffData.icon, buffData.title);
                _uiBuffs.Add(uiBuff);
            }
        }

        protected override void OnReset()
        {
            foreach (var uiBuff in _uiBuffs)
            {
                Destroy(uiBuff.gameObject);
            }

            _uiBuffs.Clear();
        }
    }
}