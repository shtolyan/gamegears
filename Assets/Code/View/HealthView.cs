using System.Collections.Generic;
using Game;
using UnityEngine;
using UnityEngine.UI;

namespace View
{
    public class HealthView : PlayerView
    {
        [SerializeField] private Image _image;
        [SerializeField] private Text _healthDeltaText;

        private RectTransform _rect;
        private readonly List<HealthDelta> _healthDeltas = new List<HealthDelta>();

        private const float _playerHeight = 3.5f;
        private const float _healthDeltaLifeTime = 0.9f;
        private const float _healthDeltaSpeed = 0.8f;

        private Vector3 WordPosition => player.Position + Vector3.up * _playerHeight;

        protected override void OnInit()
        {
            _rect = GetComponent<RectTransform>();
            _healthDeltaText.gameObject.SetActive(false);
            UpdateHealth();
        }

        protected override void OnReset()
        {
            foreach (var healthDelta in _healthDeltas)
            {
                healthDelta.Destroy();
            }

            _healthDeltas.Clear();
            UpdateHealth();
        }

        protected override void OnBuff(List<PlayerBuff> obj)
        {
            UpdateHealth();
        }

        private void Update()
        {
            if (player == null)
            {
                return;
            }

            var projection = GameController.Camera.WorldToScreenPoint(WordPosition);
            var screenPoint = new Vector2(projection.x, projection.y);
            _rect.transform.position = screenPoint;

            for (var i = _healthDeltas.Count - 1; i >= 0; i--)
            {
                var healthDelta = _healthDeltas[i];
                var isAlive = healthDelta.DoStep();
                if (!isAlive)
                {
                    _healthDeltas.Remove(healthDelta);
                }
            }
        }

        private void UpdateHealth()
        {
            _image.fillAmount = player.LifeNormalized;
            gameObject.SetActive(player.IsAlive);
        }

        protected override void OnAttack(float sealedLife)
        {
            SpawnDeltaHealth(sealedLife);
            UpdateHealth();
        }

        protected override void OnDamage(float damage)
        {
            SpawnDeltaHealth(-damage);
            UpdateHealth();
        }

        private void SpawnDeltaHealth(float deltaHealth)
        {
            if (deltaHealth == 0)
            {
                return;
            }

            var newText = Instantiate(_healthDeltaText, _rect);
            var healthDelta = new HealthDelta(newText, (int) deltaHealth, _healthDeltaLifeTime, _healthDeltaSpeed,
                GameController.Camera, WordPosition);
            _healthDeltas.Add(healthDelta);
        }
    }
}


