using UnityEngine;
using UnityEngine.UI;

namespace View
{
    public class HealthDelta
    {
        private readonly Camera _camera;
        private readonly Text _text;
        private float _lifeTime;
        private float _height;
        private Vector3 _position;
        private readonly RectTransform _rect;
        private readonly float _speed;

        public HealthDelta(Text text, int value, float lifeTime, float speed, Camera camera, Vector3 position)
        {
            _text = text;
            if (value > 0)
            {
                _text.text = $"+{value}";
                _text.color = Color.green;
            }
            else
            {
                _text.text = value.ToString();
                _text.color = Color.red;
            }

            _text.gameObject.SetActive(true);
            _lifeTime = lifeTime;
            _camera = camera;
            _position = position;
            _rect = _text.GetComponent<RectTransform>();
            _speed = speed;
            DoStep();
        }

        public bool DoStep()
        {
            _lifeTime -= Time.deltaTime;
            if (_lifeTime <= 0)
            {
                Object.Destroy(_text);
                return false;
            }

            _position += Vector3.up * _speed * Time.deltaTime;
            var projection = _camera.WorldToScreenPoint(_position);
            var screenPoint = new Vector2(projection.x, projection.y);
            _rect.transform.position = screenPoint;
            return true;
        }

        public void Destroy()
        {
            Object.Destroy(_text);
        }
    }
}