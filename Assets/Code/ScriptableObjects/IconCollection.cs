using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create Icon Collection", fileName = "IconCollection", order = 0)]
public class IconCollection : ScriptableObject
{
    [SerializeField] private List<Sprite> _sprites;

    private static IconCollection _instance;
    private Dictionary<string, Sprite> _icons;

    private static IconCollection GetInstance()
    {
        if (_instance)
        {
            return _instance;
        }

        _instance = Resources.Load<IconCollection>("IconCollection");
        _instance._icons = new Dictionary<string, Sprite>();

        foreach (var sprite in _instance._sprites)
        {
            _instance._icons.Add(sprite.name, sprite);
        }
      
        return _instance;
    }

    public static Sprite Get(string name)
    {
        return GetInstance()._icons[name];
    }

}