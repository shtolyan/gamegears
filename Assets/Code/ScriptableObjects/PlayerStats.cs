using System;
using System.Collections.Generic;
using Data;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerStats", menuName = "ScriptableObjects/CreatePlayerStats", order = 1)]
public class PlayerStats : ScriptableObject
{
    public List<PlayerStat> values;

    private void OnValidate()
    {
        var ids = new List<StatsId>();
        for (var i = values.Count - 1; i >= 0; i--)
        {
            var stat = values[i];
            if (ids.Contains(stat.id))
            {
                Debug.LogError($"Duplicated stats {stat.id}! That is not allowed.");
            }
            else
            {
                ids.Add(stat.id);
            }
        }
    }
}

[Serializable]
public class PlayerStat
{
    public StatsId id;
    public float value;
}

[Serializable]
public class PlayerBuff
{
    public int id;
    public List<PlayerStat> stats;
}