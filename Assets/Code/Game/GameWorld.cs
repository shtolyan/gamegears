using System;
using System.Collections.Generic;
using Data;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game
{
    public class GameWorld
    {
        private GameData _gameData;
        private readonly List<Player> _players;
        private static GameWorld _instance;

        private static GameWorld Instance => _instance ?? (_instance = new GameWorld());

        private GameWorld()
        {
            _players = new List<Player>();
            _instance = this;
        }

        public static void AddPlayer(Player player)
        {
            Instance._players.Add(player);
        }

        public static void DoRandomBuffs(Player player)
        {
            var buffs = new List<Buff>();

            foreach (var buff in Instance._gameData.buffs)
            {
                var chance = Random.Range(0, 2);
                if (chance == 1)
                {
                    buffs.Add(buff);
                }
            }

            player.TakeBuffs(buffs.ToArray());
        }

        public static void Load()
        {
            var textData = (TextAsset) Resources.Load("data");

            if (textData == null)
            {
                throw new NullReferenceException("Can not found data.txt!");
            }

            var gameDataJson = textData.text;
            Instance._gameData = JsonUtility.FromJson<GameData>(gameDataJson);
        }

        public static Player GetEnemy(Player player)
        {
            foreach (var candidate in Instance._players)
            {
                if (candidate != player)
                {
                    return candidate;
                }
            }

            throw new NullReferenceException("Enemy not found!");
        }

        public static Stat GetStat(StatsId id)
        {
            foreach (var stat in Instance._gameData.stats)
            {
                if (stat.id == (int) id)
                {
                    return stat;
                }
            }

            throw new NullReferenceException($"Stat with id = {id} not found!");
        }

        public static Buff GetBuff(int id)
        {
            foreach (var buff in Instance._gameData.buffs)
            {
                if (buff.id == id)
                {
                    return buff;
                }
            }

            throw new NullReferenceException($"Buff with id = {id} not found!");
        }

        public static void Clear()
        {
            foreach (var player in Instance._players)
            {
                player.Reset();
            }

            Instance._players.Clear();
        }
    }
}