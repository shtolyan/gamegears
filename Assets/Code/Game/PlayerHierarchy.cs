using UnityEngine;
using UnityEngine.UI;
using View;

namespace Game
{
    public class PlayerHierarchy : MonoBehaviour
    {
        public PlayerStats playerStats;
        public AnimatorView playerView;
        public HealthView healthView;
        public StatsView statsView;
        public BuffsView buffsView;
        public Button attackButton;
    }
}