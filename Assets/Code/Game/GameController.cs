﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private List<PlayerHierarchy> _players;
        [SerializeField] private Button _startWithoutBuffsButton;
        [SerializeField] private Button _startWithBuffsButton;
        [SerializeField] private Camera _camera;
        [SerializeField] private AudioSource _gong;

        public static Camera Camera => _instance._camera;
        private static GameController _instance;

        private void Awake()
        {
            GameWorld.Load();
            _startWithoutBuffsButton.onClick.AddListener(() => StartGame(false));
            _startWithBuffsButton.onClick.AddListener(() => StartGame(true));
            _instance = this;

            StartGame(false);
        }

        private void StartGame(bool buffs)
        {
            _gong.Play();
            GameWorld.Clear();
            foreach (var playerStat in _players)
            {
                var player = new Player(playerStat);
                GameWorld.AddPlayer(player);

                if (buffs)
                {
                    GameWorld.DoRandomBuffs(player);
                }
            }
        }
    }
}
