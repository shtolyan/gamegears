using System;
using System.Collections.Generic;
using Data;
using UnityEngine;

namespace Game
{
    public class Player
    {
        public float Life => CalculateLife();
        public float Armor => GetStat(StatsId.Armor);
        public float Damage => GetStat(StatsId.Damage);
        public float LifeSteal => GetStat(StatsId.LifeSteal);
        public float LifeNormalized => Life / CalculateMaxLife();
        public bool IsAlive => (int) Life > 0;
        public Vector3 Position => _playerHierarchy.playerView.transform.position;
        public PlayerStats Stats => _stats;
        public event Action<float> AttackEvent;
        public event Action<float> DamageEvent;
        public event Action ResetEvent;
        public event Action<List<PlayerBuff>> BuffEvent;

        private readonly PlayerStats _stats;
        private readonly List<PlayerBuff> _buffs = new List<PlayerBuff>();
        private float _receivedDamage = 0;
        private float _stolenLife = 0;
        private readonly PlayerHierarchy _playerHierarchy;
        private Action _doOnHit;

        public Player(PlayerHierarchy playerHierarchy)
        {
            _stats = playerHierarchy.playerStats;
            playerHierarchy.buffsView.Init(this);
            playerHierarchy.healthView.Init(this);
            playerHierarchy.playerView.Init(this);
            playerHierarchy.statsView.Init(this);
            playerHierarchy.attackButton.onClick.AddListener(Attack);
            _playerHierarchy = playerHierarchy;
            _playerHierarchy.attackButton.enabled = IsAlive;
        }

        private void Attack()
        {
            if (_playerHierarchy.playerView.CanAttack == false)
            {
                return;
            }

            _playerHierarchy.playerView.Attack();
            _doOnHit = () =>
            {
                var enemy = GameWorld.GetEnemy(this);
                var damageDone = enemy.GetDamage(Damage);
                var stolenLife = GetLifeSteal(damageDone);
                _stolenLife += stolenLife;
                AttackEvent?.Invoke(stolenLife);
            };
        }

        public float GetStatValue(StatsId id)
        {
            switch (id)
            {
                case StatsId.Life:
                    return Life;

                case StatsId.Armor:
                    return Armor;

                case StatsId.LifeSteal:
                    return LifeSteal;

                case StatsId.Damage:
                    return Damage;

                default:
                    throw new ArgumentOutOfRangeException(nameof(id), id, null);
            }
        }

        private float GetDamage(float damage)
        {
            var lifeBefore = Life;
            var armor = (100 - Armor) / 100;
            var armoredDamage = damage * armor;

            if (Life - armoredDamage < 0)
            {
                armoredDamage = Life;
            }

            _receivedDamage += armoredDamage;
            DamageEvent?.Invoke(armoredDamage);
            _playerHierarchy.attackButton.enabled = IsAlive;
            return lifeBefore - Life;
        }

        private float GetLifeSteal(float damageDone)
        {
            var lifeSteal = LifeSteal / 100;
            var lifeStolen = damageDone * lifeSteal;
            var maxLife = CalculateMaxLife();

            if (lifeStolen + Life > maxLife)
            {
                return maxLife - Life;
            }

            return lifeStolen;
        }

        private float CalculateMaxLife()
        {
            return GetStat(StatsId.Life);
        }

        private float CalculateLife()
        {
            var maxLife = CalculateMaxLife();
            return Mathf.Clamp(maxLife - _receivedDamage + _stolenLife, 0, maxLife);
        }

        private float GetStat(StatsId stat)
        {
            float result = 0;

            result += GetSumStatValue(_stats.values, stat);
            float sum = 0;
            foreach (var playerStat in _buffs)
            {
                sum += GetSumStatValue(playerStat.stats, stat);
            }

            result += sum;

            return result;
        }

        private float GetSumStatValue(List<PlayerStat> stats, StatsId stat)
        {
            var sum = 0f;
            foreach (var playerStat in stats)
            {
                if (playerStat.id == stat)
                {
                    sum += playerStat.value;
                }
            }

            return sum;
        }

        public void Reset()
        {
            _buffs.Clear();
            _receivedDamage = 0;
            _stolenLife = 0;
            ResetEvent?.Invoke();

            AttackEvent = null;
            DamageEvent = null;
            ResetEvent = null;
            BuffEvent = null;

            _playerHierarchy.attackButton.onClick.RemoveAllListeners();
        }

        public void TakeBuffs(Buff[] gameDataBuffs)
        {
            foreach (var gameDataBuff in gameDataBuffs)
            {
                var buff = new PlayerBuff
                {
                    id = gameDataBuff.id,
                    stats = new List<PlayerStat>()
                };

                foreach (var buffStat in gameDataBuff.stats)
                {
                    buff.stats.Add(new PlayerStat {id = (StatsId) buffStat.statId, value = buffStat.value});
                }

                _buffs.Add(buff);
            }

            BuffEvent?.Invoke(_buffs);
        }

        public void AttackComplete()
        {
            _doOnHit?.Invoke();
        }
    }
}